#include <iostream>     // For console I/O.
#include <fstream>      // For file I/O.
#include <sstream>      // String streams
#include <string>       // For strings
#include <list>         // For lists
#include <array>        // For arrays
#include <algorithm>    // For parsing stuff
using namespace std;    // std as global namespace

#include <cstring>      // For C strings
#include <cstdio>       // C stdio.h, C++'ized.
#include <cstdlib>      // Same as above.
#include <unistd.h>


int main(int argc, char *argv[]) {
	std::fstream file;
	std::string linebuffer;
	std::string bfsource;
	std::string ccode;
	bool del_sourceout;
	std::string sourceoutfile;
	std::string bfchars = "<>-+[],.";
	std::string ctemplate_begin = "#include <cstdio>\n#include <cstdlib>\n#include <iostream>\n#include <array>\n\n\nint main(void) {\n	using namespace std;\n	std::array<unsigned char, 2048> bfarea = {};\n	unsigned char *ptr = bfarea.data();\n	\n	// Version info\n	std::array<unsigned char, 16> compilername = {83, 112, 101, 101, 100, 98, 114, 97, 105, 110, 102, 117, 99, 107, 0, 0};\n	*ptr = 48;++ptr;*ptr = 46;++ptr;*ptr = 49;++ptr;*ptr = 48;++ptr;*ptr = 10;++ptr;\n	// Brainfuck code\n	";
	std::string ctemplate_end = "\n	\n}\n";
	
	// Check for correct command usage
	if (argc < 3) {
		cerr << "Usage: " << argv[0] << " <brainfuck input> <compiled output> [non-compiled output]" << endl;
		return 1;
	}
	
	// Define non-compiled output target
	if (argc > 3) {
		sourceoutfile = argv[3];
		del_sourceout = false;
	} else {
 		sourceoutfile = ".~generated-temp.cpp";
		del_sourceout = true;
	}
	
	// Open source file
	file.open(argv[1], ios::in);
	if (!file.good()) {
		cout << strerror(errno) << endl;
		return 2;
	}
	
	// Read and close source file
	while (std::getline(file, linebuffer) and !file.eof()) {
		bfsource.append(linebuffer);
	}
	file.close();
	
	// Define conversions
	std::string bf_oneleft = "<";
	std::string bf_oneright = ">";
	std::string bf_decrease = "-";
	std::string bf_increase = "+";
	std::string bf_loopstart = "[";
	std::string bf_loopend = "]";
	std::string bf_input = ",";
	std::string bf_output = ".";
	
	// Convert code to C++
	const std::string &bfcmd = "";
	for (auto &bfcmd: bfsource) {
		if      (bf_oneleft.find(bfcmd) != std::string::npos)   // <
			ccode.append("--ptr;");
		else if (bf_oneright.find(bfcmd) != std::string::npos)  // >
			ccode.append("++ptr;");
		else if (bf_decrease.find(bfcmd) != std::string::npos)  // -
			ccode.append("--*ptr;");
		else if (bf_increase.find(bfcmd) != std::string::npos)  // +
			ccode.append("++*ptr;");
		else if (bf_loopstart.find(bfcmd) != std::string::npos) // [
			ccode.append("while (*ptr != 0) {");
		else if (bf_loopend.find(bfcmd) != std::string::npos)   // ]
			ccode.append("}");
		else if (bf_input.find(bfcmd) != std::string::npos)     // ,
			ccode.append("*ptr = getchar();");
		else if (bf_output.find(bfcmd) != std::string::npos)    // .
			ccode.append("cout << static_cast<char>(*ptr) << flush;");
	}
	
	// Apply template
	ccode = ctemplate_begin + ccode + ctemplate_end;
	
	// Write generated code to file
	file.open(sourceoutfile, ios::out);
	file << ccode;
	file.close();
	
	// Run compiler
	execl("/usr/bin/env", "/usr/bin/env", "g++", sourceoutfile.c_str(), "-o", argv[2], (char*) NULL);
	
	// We are still in the program? Oops
	cerr << "Sorry, but your OS is incompatible." << endl;
	
}
